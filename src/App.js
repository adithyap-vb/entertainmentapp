import React, { useState } from "react";
import AppBar from "./components/AppBar.js";
import MusicList from "./components/MusicList";
import "./App.css";

import MusicContext from "./MusicContext";
const musicdata = require("./data.js");

function App() {
  const [musicData, setMusicData] = useState(musicdata);
  const addTrack = (track) =>
    setMusicData((musicData) => [...musicData, track]);
  const deleteTrack = (trackName) => {
    setMusicData(musicData.filter((track) => track.title !== trackName));
  };

  const value = {
    musicData,
    setMusicData,
    addTrack,
    deleteTrack,
  };
  return (
    <MusicContext.Provider value={value}>
      <div className="App">
        <AppBar />
        <MusicList />
      </div>
    </MusicContext.Provider>
  );
}

export default App;
