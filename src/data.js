const data = [
  {
    id: 1,
    like: 3,
    title: "Com Truise - Flightwave",
    subTitle: "Reed",
    media: "",
  },
  {
    id: 2,
    like: 3,
    title: "Claude Debussy - Clair de lune",
    subTitle: "Reed",
    media: "",
  },
  {
    id: 3,
    like: 2,
    title: "Culture Shock - Troglodyte",
    subTitle: "Doug",
    media: "",
  },
  {
    id: 4,
    like: 2,
    title: "Tycho - Montana",
    subTitle: "Reed",
    media: "",
  },
];

module.exports = data;
