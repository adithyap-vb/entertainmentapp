import React, { useContext, useState } from "react";
import MusicContext from "../../MusicContext";

import DeleteIcon from "@mui/icons-material/Delete";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import Typography from "@mui/material/Typography";

import "./MusicCard.css";

function MusicCard(props) {
  const { deleteTrack } = useContext(MusicContext);
  const [likes, setLikes] = useState(props.like);

  return (
    <div className="cardStyle">
      <div>
        <h2 className="titleStyle">{props.title}</h2>
        <h3 className="subTitleStyle">{props.subTitle}</h3>
      </div>
      <div className="icons">
        <figure>
          <audio controls>
            <code>audio</code>
          </audio>
        </figure>
      </div>

      <div className="actions">
        <ThumbUpIcon
          onClick={() => {
            setLikes(likes + 1);
          }}
        />
        <Typography fontSize="large" margin="5px">
          {likes}
        </Typography>
      </div>
      <DeleteIcon
        onClick={() => deleteTrack(props.title)}
        fontSize="small"
        style={{ position: "absolute", bottom: 0, right: 0 }}
      />
    </div>
  );
}

export default MusicCard;
