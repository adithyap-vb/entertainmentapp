import React, { useState } from "react";
import { TextField, Button } from "@mui/material";
function SearchBar() {
  return (
    <div style={{ display: "flex" }}>
      <TextField label="Search for Music" variant="filled" fullWidth />
      <Button>Search</Button>
    </div>
  );
}
export default SearchBar;
