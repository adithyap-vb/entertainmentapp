import React from "react";
import "./AppBar.css";

function AppBar() {
  return (
    <div>
      <h1 className="title">Music App</h1>
    </div>
  );
}

export default AppBar;
