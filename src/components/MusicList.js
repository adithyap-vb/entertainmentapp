import MusicCard from "./MusicCardComponent/MusicCard";
import React, { useContext, useState, useEffect } from "react";
import { TextField, Button } from "@mui/material";
import MusicContext from "../MusicContext";
import "./MusicList.css";
import AddTrackButton from "./AddTrackButton";
function MusicList() {
  const { musicData, setMusicData, addTrack } = useContext(MusicContext);
  const bigData = useState(musicData);

  const [searchText, setSearchText] = useState("");

  useEffect(() => {
    console.log("searchText is " + searchText);
  }, [searchText]);
  return (
    <div className="container">
      <div style={{ display: "flex", width: "900px" }}>
        <TextField
          label="Search for Music"
          variant="filled"
          id="searchBar2"
          fullWidth
        />
        <Button
          onClick={() => {
            setSearchText(document.getElementById("searchBar2").value);
          }}
          variant="contained"
        >
          Search
        </Button>
      </div>
      {searchText === "" &&
        musicData.map((item) => {
          return (
            <MusicCard
              key={item.id}
              title={item.title}
              subTitle={item.subTitle}
              like={item.like}
            />
          );
        })}
      {searchText !== "" &&
        musicData.map((item) => {
          if (item.title == searchText) {
            return (
              <MusicCard
                key={item.id}
                title={item.title}
                subTitle={item.subTitle}
                like={item.like}
              />
            );
          }
        })}
      <AddTrackButton />
    </div>
  );
}

export default MusicList;
