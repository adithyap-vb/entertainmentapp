import React, { useState, useContext } from "react";
import AddIcon from "@mui/icons-material/Add";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import Slider from "@mui/material/Slider";
import MusicContext from "../MusicContext";
import "./AddTrackButton.css";

import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  TextField,
} from "@mui/material";
function AddTrackButton() {
  const { musicData, addTrack } = useContext(MusicContext);
  const [open, setOpen] = React.useState(false);
  const [trackTitle, setTrackTitle] = useState("");
  const [trackSubTitle, setTrackSubTitle] = useState("");
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleTitle = (e) => {
    e.preventDefault();
    setTrackTitle(e.target.value);
  };
  const handleSubTitle = (e) => {
    e.preventDefault();
    setTrackSubTitle(e.target.value);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const trackObj = {
      id: musicData.length + 1,
      like: 1,
      title: trackTitle,
      subTitle: trackSubTitle,
      media: "",
    };
    setOpen(false);
    if (trackTitle !== "" && trackSubTitle !== "") addTrack(trackObj);
  };
  return (
    <div>
      <AddCircleOutlinedIcon
        className="btn"
        color="primary"
        fontSize="large"
        onClick={() => {
          handleClickOpen();
        }}
      />
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add Track</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="trackTitle"
            label="Track Title"
            type="text"
            fullWidth
            variant="outlined"
            onChange={handleTitle}
          />
          <TextField
            autoFocus
            margin="dense"
            id="trackSubTitle"
            label="Track SubTitle"
            type="text"
            fullWidth
            variant="outlined"
            onChange={handleSubTitle}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleSubmit}>Add Track</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default AddTrackButton;
