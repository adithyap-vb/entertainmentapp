import React, { createContext } from "react";
const data = require("./data.js");

// set the defaults
const MusicContext = createContext({
  musicData: data,
  addTrack: () => {},
  deleteTrack: () => {},
});

export default MusicContext;
